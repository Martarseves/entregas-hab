const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser')
const mysql = require('mysql')
const app = express();

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

//DB CONNECTION DATA
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'martars',
    password: '123456',
    database: 'bbdd_hackamarket'
})

//DB CONNECTION
connection.connect(error => {
    if (error) throw error
    console.log("DATABASE UP")
})

//PORT CONNECTION SERVICE
const PORT = 3800;

//CONNECTION SERVICE
app.listen(PORT, () => console.log("API UP"))


//PRODUCTS
//GET PRODUCTS FROM DB
app.get('/products', (req, res) => {
    //SQL SEQUENCE
    const sql = 'SELECT * FROM listaproductos'
    //DB CONNECTION
    connection.query(sql, (error, results) => {
        if (error) throw error
        if (results.length > 0) {
            res.json(results)
        } else {
            console.log("There is no products to show")
        }
    })
})
//ADD PRODUCTS TO DB
// app.post('/add-products', (req, res) => {
//     //SQL SEQUENCE
//     const sql = 'INSERT INTO listaproductos SET ?'
//     //DATA OBJECT FROM NEW PRODUCT
//     const newProduct = {
//         nombre: req.body.nombre,
//         stock: req.body.stock,
//         disponibilidad: req.body.disponibilidad,
//         imagen: req.body.imagen
//     }
//     //DB CONNECTION
//     connection.query(sql, newProduct, error => {
//         if (error) throw error
//         console.log('New product succesfully added')
//     })
// })
//UPDATE PRODUCTS IN THE DB
// app.put('/update-product/:id', (req, res) => {
//     //DATA FROM THE VIEW
//     const id = req.params.id
//     const nombre = req.body.nombre
//     const stock = req.body.stock
//     const disponibilidad = req.body.disponibilidad
//     const imagen = req.body.imagen
//     //SQL SEQUENCE
//     const sql = `UPDATE listaproductos SET nombre='${nombre}', disponibilidad='${disponibilidad}', stock='${stock}', imagen='${imagen}' WHERE id=${id}`
//     //DB CONNECTION
//     connection.query(sql, error => {
//         if (error) throw error
//         console.log("Updated product")
//     })
// })
// //DELETE PRODUCTS FROM DB
// app.delete('/delete-product/:id', (req, res) => {
//     //DATA FROM THE VIEW
//     const id = req.params.id
//     //SQL SEQUENCE
//     const sql = `DELETE FROM listaproductos WHERE id=${id}`
//     //DB CONNECTION
//     connection.query(sql, error => {
//         if (error) throw error
//         console.log("Product deleted")
//     })
// })



//CLIENTS
//GET CLIENTS FROM DB
app.get('/clients', (req, res) => {
    //SQL SEQUENCE
    const sql = 'SELECT * FROM listaclientes'
    //DB CONNECTION
    connection.query(sql, (error, results) => {
        if (error) throw error
        if (results.length > 0) {
            res.json(results)
        } else {
            console.log("There is no clients to show")
        }
    })
})
//ADD CLIENTS TO DATABASE
app.post('/register', (req, res) => {
    //SQL SEQUENCE
    const sql = 'INSERT INTO listaclientes SET ?'
    //DATA OBJECT FROM NEW CLIENT
    const newClient = {
        nombre: req.body.nombre,
        usuario: req.body.usuario,
        password: req.body.password,
        email: req.body.email,
        foto: req.body.foto
    }
    //DB CONNECTION
    connection.query(sql, newClient, error => {
        if (error) throw error
        console.log('New client succesfully added')
    })
})
//UPDATE CLIENTS IN THE DB
app.put('/clients/update-client/:id', (req, res) => {
    //DATA FROM THE VIEW
    const id = req.params.id
    const nombre = req.body.nombre
    const usuario = req.body.usuario
    const password = req.body.password
    const email = req.body.email
    const foto = req.body.foto
    //SQL SEQUENCE
    const sql = `UPDATE listaclientes SET nombre='${nombre}', usuario='${usuario}', password='${password}', email='${email}', foto='${foto}' WHERE id=${id}`
    //DB CONNECTION
    connection.query(sql, error => {
        if (error) throw error
        console.log("Updated client")
    })
})
//DELETE CLIENTS FROM DB
app.delete('/clients/delete/:id', (req, res) => {
    //DATA FROM THE VIEW
    const id = req.params.id
    //SQL SEQUENCE
    const sql = `DELETE FROM listaclientes WHERE id='${id}'`
    //DB CONNECTION
    connection.query(sql, error => {
        if (error) throw error
        console.log("Deleted client")
    })
})

