USE entregasql;

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE IF NOT EXISTS users (
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(50),
surnames VARCHAR(50),
dni VARCHAR(9) UNIQUE NOT NULL,
email VARCHAR(50),
telephone INT,
account_number INT NOT NULL
)

CREATE TABLE IF NOT EXISTS vechicles (
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
model VARCHAR(50),
brand VARCHAR(50),
plate_number VARCHAR(7) NOT NULL,
type ENUM('Coche', 'Motocicleta','Furgoneta')
)

CREATE TABLE IF NOT EXISTS registries (
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
initial_park_time DATETIME,
final_park_time DATETIME,
payment DECIMAL(4,2)
)

CREATE TABLE IF NOT EXISTS districts (
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(50),
initial_time DATETIME,
final_time DATETIME,
price_per_minute DECIMAL (4,2),
time_limit INT,
observation ENUM ('Operativo', 'No operativo: en obras'),
id_registry INT UNSIGNED,
FOREIGN KEY (id_registry) REFERENCES registries (id)
)

CREATE TABLE IF NOT EXISTS penalties (
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
quantity DECIMAL (5,2),
id_registry INT UNSIGNED,
FOREIGN KEY (id_registry) REFERENCES registries (id),
id_reclamation INT UNSIGNED,
FOREIGN KEY (id_reclamation) REFERENCES reclamations (id)
)

CREATE TABLE IF NOT EXISTS reclamations (
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
processing ENUM ('Tramitada','En trámite'),
status ENUM ('Aceptada', 'Rechazada')
motive VARCHAR(500),
date DATE,
id_user INT UNSIGNED,
FOREIGN KEY (id_user) REFERENCES users (id)
)

CREATE TABLE IF NOT EXISTS users_vehicles (
id_user INT UNSIGNED,
FOREIGN KEY (id_user) REFERENCES users (id),
id_vehicle INT UNSIGNED,
FOREIGN KEY (id_vehicle) REFERENCES vehicles (id)
)

CREATE TABLE IF NOT EXISTS users_registries (
id_user INT UNSIGNED,
FOREIGN KEY (id_user) REFERENCES users (id),
id_registry INT UNSIGNED,
FOREIGN KEY (id_registry) REFERENCES registries (id)
)





SET FOREIGN_KEY_CHECKS=1;