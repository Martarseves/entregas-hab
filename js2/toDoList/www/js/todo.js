"use strict;";

let addNewTask = document.getElementById("newtask"),
  addButton = document.getElementsByTagName("button")[0],
  incompleteTaskContainer = document.getElementById("incomplete"),
  completedTasksContainer = document.getElementById("completed");

let createNewTask = function (tsk) {
  let listTasks = document.createElement("li"),
    checkBox = document.createElement("input"),
    label = document.createElement("label"),
    deleteButton = document.createElement("button");

  checkBox.type = "checkbox";
  checkBox.className = "checkbox";

  deleteButton.innerText = "Borrar";
  deleteButton.className = "delete";

  label.innerText = tsk;

  listTasks.appendChild(checkBox);
  listTasks.appendChild(label);

  listTasks.appendChild(deleteButton);

  return listTasks;
};

let addTask = function () {
  let listTasksName = addNewTask.value;
  let listTasks = createNewTask(listTasksName);

  incompleteTaskContainer.appendChild(listTasks);
  taskStatus(listTasks, taskCompleted);

  addNewTask.value = ""; // resetea
};

addButton.addEventListener("click", addTask);

let deleteTask = function () {
  // facendo uso de node
  let listTasks = this.parentNode; // .parentNode => referencia ao obxecto que contén ao botón
  let ul = listTasks.parentNode; // .parentNode => referencia á lista que contén á tarefa

  ul.removeChild(listTasks); // .removeChild => elimina a tarefa da lista ppal do ul
};

let taskStatus = function (tasklistTasks, checkBoxStatus) {
  let checkBox = tasklistTasks.querySelector("input.checkbox");
  let deleteButton = tasklistTasks.querySelector("button.delete");
  deleteButton.onclick = deleteTask;
  checkBox.onchange = checkBoxStatus;
};

let taskCompleted = function () {
  let listTasks = this.parentNode; // referncia á rama ppal listtask

  completedTasksContainer.appendChild(listTasks);

  taskStatus(listTasks, taskIncomplete);
  console.log(completedTasksContainer.childNodes);
};

//eliminar tarefas completadas
let deleteAll = document.createElement("button");
deleteAll.className = "deleteallbutton";
deleteAll.innerText = "Borrar tudo";
completedTasksContainer.appendChild(deleteAll);

let deleteAllCompleted = function () {
  let childrenNodes = completedTasksContainer.childNodes;
  let completedTasks = [];
  childrenNodes.forEach((element) => {
    if (element.className !== "deleteallbutton") {
      completedTasks.push(element);
    }
  });
  completedTasks.forEach((value) => {
    completedTasksContainer.removeChild(value);
  });
};

deleteAll.onclick = deleteAllCompleted;

let taskIncomplete = function () {
  let listTasks = this.parentNode; // referencia á rama ppal listtask

  incompleteTaskContainer.appendChild(listTasks);
  taskStatus(listTasks, taskCompleted);
};
