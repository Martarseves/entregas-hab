function currentTime() {
  let date = new Date();
  let hour = date.getHours();
  let min = date.getMinutes();
  let sec = date.getSeconds();
  hour = updateTime(hour);
  min = updateTime(min);
  sec = updateTime(sec);

  document.getElementById("clock").innerText = hour + ":" + min + ":" + sec;
  let dt = setTimeout(function t() {
    currentTime();
  }, 1000);

  if (hour < 12) {
    document.getElementById("morning").innerText = "buenos dias con ausonia";
  } else if (hour >= 12 && hour < 20) {
    document.getElementById("afternoon").innerText = "el asfalto esta que arde";
  } else {
    document.getElementById("night").innerText = "bendiciones y buenas noches";
  }

  if (hour === 12 && min === 29) {
    clearTimeout(dt);
  }
}

function updateTime(k) {
  if (k < 10) {
    return "0" + k;
  } else {
    return k;
  }
}

currentTime();

/* OTRA FORMA

  let alarmDate = new Date();
  let alarmHour = alarmDate.getHours();
  let alarmMin = alarmDate.getMinutes();
  if (alarmDate >= date) {
    clearTimeout(dt);
  }

  */
