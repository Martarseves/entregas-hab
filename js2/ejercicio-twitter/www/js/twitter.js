let input = document.getElementById("tweet");
let list = document.getElementById("tweets");
let addButton = document.getElementsByClassName("button")[0];

let newTweet = function (event) {
  event.preventDefault();

  let li = document.createElement("li"),
    p = document.createElement("p"),
    footer = document.createElement("footer"),
    time = document.createElement("time"),
    deleteButton = document.createElement("button");

  const now = new Date();
  time.textContent = `${now.getDate()}/${
    now.getMonth() + 1
  }/${now.getFullYear()}`;

  deleteButton.className = "deletebutton";
  deleteButton.textContent = "Borrar tweet";

  txt = input.value;
  p.innerText = txt;
  input.value = "";

  footer.appendChild(time);
  footer.appendChild(deleteButton);

  list.appendChild(li);
  li.appendChild(p);
  li.appendChild(footer);
  footer.appendChild(time);
  footer.appendChild(deleteButton);
  console.log(li);
};

addButton.addEventListener("click", newTweet);

let deleteTweet = function (tweetlist) {
  let li = this.parentNode;
  let ul = li.parentNode;

  let deleteButton = tweetlist.querySelector("button.delete");
  deleteButton.onclick = deleteTask;

  ul.removeChild(li);
};
