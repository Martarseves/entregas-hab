const fs = require("fs").promises;
const path = require("path");
const minimist = require("minimist");
//const file=path.join()

const {
  addTodo,
  markAsDone,
  markAsUndone,
  listTodos,
  cleanTodos,
} = require("./lib/actions");

async function todoList() {
  const args = minimist(process.argv.slice(2));
  console.log;

  const { _, priority, list, done, clean, undone } = args;

  if (list) {
    await listTodos();
    process.exit();
  }
  if (done) {
    await markAsDone(done);
    process.exit();
  }
  if (clean) {
    await cleanTodos();
    process.exit();
  }
  if (undone) {
    await markAsUndone(undone);
    process.exit();
  }

  const newTodo = _.join(" ");

  if (newTodo.length > 0) {
    await addTodo({
      text: newTodo,
      priority: priority ? true : false,
    });
    process.exit;
  } else {
    console.log("No hay tarea");
  }
}
todoList();
