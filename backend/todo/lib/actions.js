const fs = require("fs").promises;
const path = require("path");
const os = require("os");
const { formatDistanceToNow } = require("date-fns");
const { es } = require("date-fns/locale");
const chalk = require("chalk");

const file = path.join(os.homedir(), ".tasks.json");

async function readTodoList() {
  try {
    let todos;
    try {
      const todoListContent = await fs.readFile(file);
      todos = JSON.parse(todoListContent.toString());
    } catch (e) {
      todos = { todos: [] };
      await fs.writeFile(file, JSON.stringify(todos));
    }
    return todos;
  } catch (e) {
    console.error(error);
  }
}

async function saveTodos(todoList) {
  try {
    await fs.writeFile(file, JSON.stringify(todoList));
  } catch (error) {
    console.error(error.message);
  }
}

async function addTodo(data) {
  const { text, priority } = data;
  const currentList = await readTodoList();
  const creationDate = new Date();

  currentList.todos.unshift({
    text,
    priority,
    done: false,
    creationDate,
  });
  //const creationDate = now.toISOString();
  console.log(`Se ha añadido la tarea ${currentList.todos[0].text}`);

  await saveTodos(currentList);
}

async function markAsDone(index) {
  try {
    const currentList = await readTodoList();
    currentList.todos[index - 1].done = true;

    console.log(`La tarea ${index} ha sido realizada`);

    await saveTodos(currentList);
  } catch (e) {
    console.error("Índice erróneo");
  }
}

async function markAsUndone(index) {
  try {
    const currentList = await readTodoList();

    currentList.todos[index - 1].done = false;

    await saveTodos(currentList);
  } catch (e) {
    console.error("Índice erróneo");
  }
}

async function listTodos() {
  try {
    const data = await readTodoList();
    const list = data.todos;
    if (list.length === 0) {
      console.error("Ninguna tarea por realizar");
    } else {
      console.log(chalk.underline("LISTA DE TAREAS:"));

      for (const task of list) {
        if (task.priority === false) {
          task.text = `${task.text}`;
          task.priority = "";
        } else {
          task.text = chalk.magenta.bold(`${task.text}`);
          task.priority = chalk.magenta("alta prioridad");
        }
        if (task.done === true) {
          task.done = chalk.grey("tarea realizada");
        } else {
          task.done = chalk.yellow("tarea pendiente");
        }

        const humanDistance = formatDistanceToNow(new Date(task.creationDate), {
          locale: es,
        });

        console.log(
          `${list.indexOf(task) + 1}: ${task.text} (${task.done}${
            task.priority
          })- ${humanDistance}`
        );
      }
    }
  } catch (error) {
    console.error("No se pueden listar las tareas");
    console.error(error);
  }
}

async function cleanTodos() {
  try {
    const currentList = await readTodoList();
    const undoneTodos = currentList.todos.filter((task) => {
      return task.done === false || !task.done;
    });
    currentList.todos = undoneTodos;

    console.log("Las tareas ya realizadas han sido borradas");
    await saveTodos(currentList);
  } catch (e) {
    console.error("Error al intentar borrar");
    console.log(e);
  }
}

module.exports = {
  addTodo,
  markAsDone,
  markAsUndone,
  listTodos,
  cleanTodos,
};
