"use strict";
/* 
let nEmpleados = 3;

if (nEmpleados <= 50) {
  console.log("empresa pequenha");
} else if (nEmpleados > 50 && nEmpleados < 150) {
  console.log("empresa mediana");
} else if (nEmpleados > 150) {
  console.log("empresa grande");
}

const Lista = ["A", "B", "C"];

function mostrar(arrayLista) {
  for (let i = 0; i < 3; i++) {
    console.log(arrayLista[i]);
  }
}

mostrar(Lista);


function nPrimo(n) {
  for (let i = 2; i < n; i++) {
    if (n % i === 0) {
      return false;
    }
    return true;
  }
}
function primList(limit) {
  for (let i = 2; i <= limit; i++) {
    if (nPrimo(i)) {
      console.log(i);
    }
  }
}

primList(50);

//ejercicio piramides//

//ejercicio 1//

function pyramid(numberOfFloors) {
  let stringy = "";
  for (let i = 0; i < numberOfFloors; i++) {
    stringy = stringy + "*";
    console.log(stringy);
  }
}
pyramid(5);


function buildFloor(size) {
  let floor = '';
  for (let i = 0; i < size; i++) {
    floor = floor + '*';
  }
  return floor;
}

function printPyramid1(floors) {
  for (let i = 0; i < floors; i++) {
    console.log(buildFloor(i + 1));
  }
}

console.log(printPyramid1(5));



function pyramid3(numberOfFloors3) {
  let stringy3 = "*****";
  for (let i = 0; i < 5; i++) {
    showStars(i);
  }
}
pyramid3(5);


//piramide inversa //

function showStars(numberStars) {
  let stars = "";
  for (let i = 0; i < numberStars; i++) {
    stars = stars + "*";
  }
  console.log(stars);
}

function pyramid(numberOfFloors) {
  let stringy = "*****";
  for (let i = 5; i > 0; i--) {
    showStars(i);
  }
}
pyramid(5);


//piramide 2//

function buildFloor(size, brick) {
  let floor = "";
  for (let i = 0; i < size; i++) {
    floor = floor + brick;
  }
  return floor;
}

function printPyramid1(height) {
  for (let i = 0; i < height; i++) {
    console.log(buildFloor(height - (i + 1), " ") + buildFloor(i + 1, "*"));
  }
}

console.log(printPyramid1(5));



//piramide 3//

function piso(repeticiones, simbolo) {
  const miPiso = "";
  for (let i = 0; i < repeticiones; i++) {
    miPiso = miPiso + simbolo;
  }
  return miPiso;
}

function construirPiramide(altura) {
  for (let i = 0; i < altura; i++) {
    console.log(piso(altura - 1 - i, " ") + piso(2 * i + 1, "*"));
  }
}

construirPiramide(5);

//piramide 4//

function piso(repeticiones, simbolo) {
  let miPiso = "";
  for (let i = 0; i < repeticiones; i++) {
    miPiso = miPiso + simbolo;
  }
  return miPiso;
}

function construirPiramide(altura) {
  for (let i = 0; i < altura; i++) {
    console.log(piso(altura - 1 - i, " ") + piso(i, "*"));
  }
  for (let i = altura - 1; i > 0; i--) {
    console.log(piso(altura - i, " ") + piso(2 * 1 - 1, "*"));
  }
}

construirPiramide(5);


//Fibonacci sequence//

function fibonacci(n) {
  let a = 0,
    b = 1,
    c,
    d = 1;
  console.log(a, b);
  for (let i = 3; i <= n; i++) {
    c = a + b;
    console.log(c);
    d = d + c;
    a = b;
    b = c;
  }
}
fibonacci(prompt("Cantidad deseada de numeros de la secuencia:"));

//otra forma//

function fibonacci(n) {
  if (n <= 1) {
    return n;
  }
  return fibonacci(n - 1) + fibonacci(n - 2);
}
console.log(fibonacci(7));

//usando memo//
let memo = { 0: 0, 1: 1 };
function fibonacciOpt(n) {
  if (memo[n] || n === 0) {
    return memo[n];
  }
  const computedValue = fibonacciOpt(n - 1) + fibonacciOpt(n - 2);
  memo[(n = computedValue)];
  return computedValue;
}
console.time();
console.log(fibonacci);
console.timeEnd();
console.time();
console.log(fibonacciOpt(), fibonacciOpt());
console.timeEnd();

//Defuse bombs//

function defuseCode() {
  return Math.round(Math.random() * 9 + 1);
}

const realCode = defuseCode();

function myDefuseBomb() {
  for (let i = 0; i < 5; i++) {
    let code = +prompt(realCode);
    if (code === realCode) {
      return "Defused bomb";
    }
    console.log(`${4 - i} trys`);
  }
  return "BOOM!";
}

const result = myDefuseBomb();

console.log(result);



let direccionUsuario = {
  calle: "ROnda",
  numero: "11"
};

let myObject = {};
console.log(myObject);
let person = {
  name: "Ivan",
  age: 35,
  genre: "masculino",
  direccion: direccionUsuario,
  saludo: function() {
    console.log("hola");
  }
};

let persoaConDireccion = person;
console.log(person);
persoaConDireccion.name = "Lucia";
const campo = "name";
console.log(persoaConDireccion[campo]);

const person2 = { ...person };
person2.direccion = { ...person.direccion };

let a = {};




const arrayVacio = [3, 4, 5, 6, 7];

arrayVacio.push(4);
arrayVacio.unshift(10);
const valorQuitado = arrayVacio.pop();
const valorQuitado2 = arrayVacio.shift();
const posiciondel5 = arrayVacio.indexOf(5);

console.log(arrayVacio, valorQuitado, valorQuitado2, posiciondel5);

for (let index = 0; index < arrayVacio.length; index++) {
  console.log(arrayVacio[index]);
}
*/

// ejercicio infectados --MIRAR MAP
/*
const patients = [true, false, true, false, false, false, true, true];

function infect(patients) {
  let infected = [...patients];

  for (let i = 0; i < patients.length; i++) {
    if (patients[i]) {
      if (i - 1 > 0) {
        infected[i - 1] = true;
      }
      if (i + 1 < infected.length) {
        infected[i + 1] = true;
      }
    }
  }

  return infected;
}

console.log(patients, infect(patients));


const patients = [true, false, true, false, false, false, true, true];

function infect(patients) {
  let infected = [...patients];

  for (let i = 0; i < patients.length; i++) {
    if (patients[i]) {
      if (i - 1 > 0) {
        infected[i - 1] = true;
      }
      if (i + 1 < infected.length) {
        infected[i + 1] = true;
      }
    }
    break;
  }

  for (let i = 0; i < patients.length; i++) {
    if (patients[i]) {
      infected[i] = false;
    }
  }
  return infected;
}

console.log(patients, infect(patients));


//funcion del tamaño de un cuadrado usando arrays

let matrix = new Array(7);
for (let i = 0; i < matrix.length; i++) {
  matrix[i] = new Array(7).fill(" ");
}
for (let i = 0; i < matrix.length; i++) {
  matrix[i][i] = "*";
  matrix[matrix.length - 1 - i][i] = "-";
}

console.log(matrix);

//otra forma

function createMatrix(size) {
  let matrix = new Array(size);
  for (let i = 0; i < matrix.length; i++) {
    matrix[i] = new Array(size).fill(" ");
  }
  return matrix;
}
let myMatrix = createMatrix(7);


const animal = { name: 'gallina' }
const establo = [{ name: 'zorro' }, { name: 'gallina' }]

function granja(establo) {
  for (let i = 0; i < 2; i++){
    establo.push(getRandomAnimal());
  }
  if (animal === establo.name) {
    let quequeda = [{ name: 'gallina' }, { name: 'gallina'}, { name: 'gallina'}]
  } 
  else if (animal!==establo.name) {
    let quequeda2={name:'zorro'}
  } 
    
  
return granja();
}
granja(establo);

function granja(establo) {
  if (establo[0].animal !== establo[1].animal) {
    return establo.filter( (item) => { return item.animal=== 'zorro';})}
  }

/*array con las personas y el objeto de la persona tiene a mayores todos los datos de su mascota



const names = ["Willy", "Ivan", "Ramiro"];
const eyeColor = ["azul", "marron", "azul"];
const height = ["bajo", "alto", "alto"];
const tattooed = [true, false, false];
const tip = [
  {
    height: "alto",
  },
  {
    eyeColor: "marron",
  },
  {
    tattooed: false,
  },
];

const suspectOne = [
  { name: "Willy" },
  { eyeColor: "azul" },
  { height: "bajo" },
  { tattooed: true },
  { tip: { height: "alto" } },
];
const suspectOTwo = [
  { name: "Ivan" },
  { eyeColor: "marron" },
  { height: "alto" },
  { tattooed: false },
  { tip: { eyeColor: "marron" } },
];
const suspectThree = [
  { name: "Ramiro" },
  { eyeColor: "azul" },
  { height: "alto" },
  { tattooed: false },
  { tip: { tattooed: false } },
];

class Person {
  constructor(name) {
    this.name = name;
  }
}
class Detective extends Person {
  suspects = {};
  summary = {};
  constructor(name) {
    super(name);
  }
  startInvestigation(names, eyeColor, height, tattooed, tip) {
    this.suspects = suspect.createList(names, eyeColor, height, tattooed, tip);
  }
  Data() {
    for (let i = 0; i < this.suspects.length; i++) {
      const confession = this.suspects[i].confess();
      this.summary = { ...this.summary, ...confession };
    }
  }
}
class Suspect extends Person {
  name;
  eyeColor;
  height;
  tattooed;
  #tip;
  constructor(name, tip) {
    this.#tip = tip;
  }
  confess() {}
}
const suspect = names.map((name, index) => {
  return new Suspect();
});

const mySuspect = newSuspect;

class suspect {
  constructor(suspectData) {}
}




/*Crear un falso e-commerce. Por un lado tenemos todos los artículos que forman
el stock de la tienda.Tendremos también un usuario que añade cosas a su carrito,
que es privado.Los artículos cuando los metemos al carrito los convertimos un tipo de
dato que guarda las unidades del usuario de dicho artículo.La tienda es la
encargada de imprimir un ticket por la consola.*/

//ejercicio hecho por Ivan
/*
const itemNames = ["Camisa", "Pantalon", "Calcetines"];
const itemPrizes = [13, 27, 100];

class Item {
  name;
  prize;

  constructor(name, prize) {
    this.name = name;
    this.prize = prize;
  }
}

class CartItem extends Item {
  ammount = 1;
  itemData;

  constructor(item) {
    super(item.name, item.prize);
  }
  increaseAmmount() {
    this.ammount++;
  }
}
class User {
  #shoppingCart = [];
  addToCart(item) {
    const itemFound = this.#shoppingCart.find((cartItem) => {
      return item.name === cartItem.itemData.name;
    });
    if (itemFound) {
      itemFound.increaseAmmount();
    } else {
      this.#shoppingCart.push(new CartItem(item));
    }
  }

  get cart() {
    return this.#shoppingCart;
  }
  fillCart(inventary, ammount) {
    for (let i = 0; i < ammount; i++) {
      const inventaryIndex = Math.floor(Math.random() * inventary.length);
      this.addToCart(inventary[inventaryIndex]);
    }
  }
}

const inventary = itemNames.map((name, index) => {
  return new Item(name, itemPrizes[index]);
});

const myUser = new User();
myUser.fillCart(inventary, 10);

console.log(myUser, myUser.cart);


const itemNames = ["Camisa", "Pantalon", "Calcetines"];
const itemPrices = [13, 27, 100];
//const items = Item.buildItems(itemNames, itemPrices);
//const myUser = new User();

class Item {
  name;
  prize;

  constructor(name, prize) {
    this.name = name;
    this.prize = prize;
  }
}

// elaboramos un array con todos los items a la venta y sus precios:

const inventary = itemNames.map((name, prize) => {
  return new Item(name, prize[index]);
});

class User {
  #carrito = [];

  addItems(item) {
    let foundItem = this.#carrito.find((element) => {
      return element.itemData.itemType === item.itemType;
    });
    if (foundItem) {
      foundItem.ammount++;
    } else {
      this.#carrito.push({ itemData: item, ammount: 1 });
    }

    // Primero busco si está en el carrito

    // Si no habia, lo meto
    //else {s
    //Si existia le incremento las unidades
    //
  }

  leerCarrito() {
    return this.#carrito;
  }
}

class Tienda {

}

const itemNames = ["Camisa", "Pantalon", "Calcetines"];
const itemPrices = [13, 27, 100];
//contabiliadad carrito usuario

class Item {
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }
  static createItem(names, prices) {
    return names.map((name, index) => {
      return new Item(names[index], prices[index]);
    });
  }
}

// Lista de items
const itemList = Item.createItem(itemNames, itemPrices);
console.log(itemList);

class User {
  shoppingCart = [];
  constructor(name) {
    this.name = name;
  }
  addToCart(limit, items) {
    for (let i = 0; i < limit; i++) {
      const itemIndex = Math.round(Math.random() * itemList.length);
      this.shoppingCart.push(items[itemIndex]);
    }
    let foundItem = this.shoppingCart.find((element) => {
      return element.itemData.itemType === item.name;
    });
    if (foundItem) {
      foundItem.ammount++;
    } else {
      this.shoppingCart.push({ itemData: item, ammount: 1 });
    }

  }
}

class ShoppingCar {

}

const myUser = new User();
myUser.addToCart(5, itemList);
console.log(myUser.shoppingCart);







/*





const myUser = new User();
myUser.addItems(inventary[0]);
myUser.addItems(inventary[0]);
myUser.addItems(inventary[0]);
myUser.addItems(inventary[1]);
myUser.addItems(inventary[2]);
myUser.addItems(inventary[2]);
myUser.addItems(inventary[2]);
myUser.addItems(inventary[2]);
myUser.addItems(inventary[2]);
console.log(myUser);
*/

// quiero tener una funcion que haga de cronometro
// la funcion va a tener un parametro ue indica en segundos,
// cuando quiero que me avise.
// al fianl del coronometro, saca algo por la consola
// cada segundo me saca por la consola cuanto tiempo ha pasado.
/*
function chronometer(timeLimit) {
  let i = 0;
  let t = timeLimit;
  const sec = setInterval(() => {
    if (t === i) {
      console.log(`${t} seconds`);
    } else {
      i++;
      console.log(`${t} seconds`);
    }
  }, 1000);
}

chronometer();

function chronometer2(duration) {
  let elapsed = 0;
  const intervalId = setInterval(() => {
    console.log(`${elapsed}s`);
    elapsed++;
    if (elapsed > duration) {
      clearInterval(intervalId);
    }
  }, 1000);
}

chronometer2(5);


function promiseGenerator() {
  const randomNumber = Math.round(Math.random() * 5000);
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(randomNumber);
    }, randomNumber);
  });
}

async function myFunction(params) {
  const myPromise1 = promiseGenerator();
  const myPromise2 = promiseGenerator();
  const myPromise3 = promiseGenerator();
  const value1 = await myPromise1;
  console.log("value1", value1);
  const value2 = await myPromise2;
  console.log("value2", value2);
  const value3 = await myPromise3;
  console.log("value3", value3);
}

myFunction();




//ES UNA NOTACION SOLO PARA OBJETOS

const user = { a: { b: {} } };
const userCopy = { ...user };

console.log(user.a === userCopy.a); //true

const userCopy2 = JSON.parse(JSON.stringify(user));
//esto ultimo hace que las referencias a "a"o "b" de la copia
//no sean iguales al original--> es un objeto nuevo
//convierte el objeto a texto y luego a objeto otra vez
|

// como pasar objeto a texto, objeto a json

const stringfiedObject = JSON.stringify(user);

console.log(user, stringfiedObject);

//viceversa

const parsedObject = JSON.parse(stringfiedObject);

console.log(parsedObject);


//fetch me devuelve una promesa
fetch("https://anapioficeandfire.com/api/characters/583").then((response) => {
  console.log(response);
  return response.json().then((value) => {
    console.log(value);
  });
});

//Recuperar los datos de la casa Targaryen de juego de la API de Game of Thrones,
//de esta casa deberemos averiguar cual es el Lord actual (almacenado en currentLord) 
//y recuperar sus datos, los del lord.
//Del lord actual debemos sacar por la consola el nombre (name), y los titulos (titles)
// cada uno en una linea por consola.
//Enlace para hacer fetch https://www.anapioficeandfire.com/api/houses/378



fetch("https://www.anapioficeandfire.com/api/houses/378").then((response) => {
  response.json().then((houseData) => {
    fetch(houseData.currentLord).then((response) => {
      response.json().then((lordData) => {
        console.log("Name", lordData.name);
        console.log("Titles");
        for (const title of lordData.titles) {
          console.log(`- ${title}`);
        }
      });
    });
  });
});




async function getHouse(targaryenLord) {
  const requestHouse = await fetch(
    `https://www.anapioficeandfire.com/api/houses/378`
  );
  const houseData = await requestHouse.json();
  const requestLord = await fetch(houseData.currentLord);
  const lordData = await requestLord.json();
  console.log(lordData.name);
  for (const title of lordData.titles) {
    console.log(`- ${title}`);
  }
}

getHouse();






//
async function getInfo() {
  const requestInfo = await fetch('https://www.anapioficeandfire.com/api/houses/378');

  const infoData = await requestInfo.json();

  console.log(infoData);

  for (let i = 0; i < 3; i++) {
    let aleatory = Math.round(Math.random() * infoData.swornMembers.length);
    console.log(infoData.swornMembers[aleatory]);
  }
}

getInfo();

const allPromises = [];
allPromises.push(getInfo());
Promise.all(allPromises).then((allData) => {
  console.log(allData);
  return;
});

*/

/*
- Recuperar los datos de tres personajes al azar del array de "swornMembers"
 de la primera petición. Estos datos se tienen que mostrar todos a la vez.
- Recuperar los datos de otros tres personajes al azar, pero como Daenerys
 se ha levantado con mal pie, el primero en llegar del servidor lo mandaremos 
 para el otro barrio diciendo su nombre y DRACARYS por la consola.


//Primera parte

/*
function randomPosition(data) {
  return Math.round(Math.random() * data.length);
}

async function getSworn() {
  const swornRequest = await fetch(
    "https://www.anapioficeandfire.com/api/houses/378"
  );
  const swornData = await swornRequest.json();
  const chosenSwornMembers = [];
  for (let i = 0; i < 3; i++) {
    chosenSwornMembers.push(
      fetch(swornData.swornMembers[randomPosition(swornData.swornMembers)])
    );
  }
  const allRequest = await Promise.all(chosenSwornMembers);
  const allData = allRequest.map(async (request) => {
    return await request.json();
  });
  for (const member of allData) {
    const memberData = await member;
    console.log(memberData.name);
  }
}

getSworn();

//Segunda parte

function randomPosition(data) {
  return Math.round(Math.random() * data.length);
}

async function getSworn() {
  const swornRequest = await fetch(
    "https://www.anapioficeandfire.com/api/houses/378"
  );
  const swornData = await swornRequest.json();
  const chosenSwornMembers = [];
  for (let i = 0; i < 3; i++) {
    chosenSwornMembers.push(
      fetch(swornData.swornMembers[randomPosition(swornData.swornMembers)])
    );
  }
  const allRequest = await Promise.race(chosenSwornMembers);
  const allData = await allRequest.json();

  console.log(allData.name + " DRACARYS");
}

getSworn();

function changeDivisas(data) {
  return data;
}
*/

async function getDineritosE() {
  const dinerosEuros = await fetch(
    "https://api.exchangerate-api.com/v4/latest/EUR"
  );
  const dinerosQueSalen = await dinerosEuros.json();
  const pelasEu = await fetch(dinerosQueSalen.rate);
  const conv = await fetch(pelasEu.EUR);

  console.log(conv);
}
getDineritosE(8);
