"use strict";
/*
// ejercicio numero 1

let numero1 = parseInt(prompt());
let numero2 = parseInt(prompt());
let operacion = prompt();

//utilizando else e if

function calculadora() {
  if (operacion === "+") {
    console.log(`el resultado de la suma es ${numero1 + numero2}`);
  } else if (operacion === "-") {
    console.log(`el resultado de la resta es ${numero1 - numero2}`);
  } else if (operacion === "*") {
    console.log(`el resultado de la multiplicacion es ${numero1 * numero2}`);
  } else if (operacion === "/") {
    console.log(`el resultado de la division  es ${numero1 / numero2}`);
  } else if (operacion === "**") {
    console.log(`el resultado de la potencia es ${numero1 ** numero2}`);
  }
}

calculadora(4, 2);

//utilizando switch

function calculadora2() {
  switch (operacion) {
    case "+":
      console.log(`el resultado de la suma es ${numero1 + numero2}`);
      break;
    case "-":
      console.log(`el resultado de la resta es ${numero1 - numero2}`);
      break;
    case "*":
      console.log(`el resultado de la multiplicacion es ${numero1 * numero2}`);
      break;
    case "/":
      console.log(`el resultado de la division es ${numero1 / numero2}`);
      break;
    case "**":
      console.log(`el resultado de la potencia es ${numero1 ** numero2}`);
      break;
  }
}
calculadora2();
*/
//ejercicio numero 2

let equipoMaria = {
  puntos: (62 + 34 + 55) / 3,
};

let equipoPaula = {
  puntos: (35 + 60 + 59) / 3,
};

let equipoRebeca = {
  puntos: (40 + 39 + 63) / 3,
};

if (
  equipoMaria.puntos == equipoPaula.puntos &&
  equipoMaria.puntos == equipoRebeca.puntos
) {
  console.log(
    "El equipo de Maria, el de Paula y el de Rebeca tienen los mismos puntos"
  );
} else {
  if (equipoMaria.puntos > equipoPaula.puntos) {
    if (equipoMaria.puntos > equipoRebeca.puntos) {
      console.log("El equipo de Maria tiene mayor puntuacion media");
    } else {
      console.log("El equipo de Rebeca tiene mayor puntuacion media");
    }
  } else {
    if (equipoMaria.puntos < equipoPaula.puntos) {
      if (equipoPaula.puntos > equipoRebeca.puntos) {
        console.log("El equipo de Paula tiene mayor puntuacion media");
      } else {
        console.log("El equipo de Rebeca tiene mayor puntuacion media");
      }
    }
  }
}
//ejercicio numero 3
/*
function dado() {
  return Math.floor(Math.random() * 6) + 1;
}

function tiradaDado() {
  let acum = 0;
  for (let i = 1; i <= 50; i++) {
    const miDado = dado();
    acum = acum + miDado;
    if (acum < 50) {
      console.log(
        `Tirada ${i}: ha salido un ${miDado}.Tienes un total de ${acum} puntos`
      );
    } else {
      console.log(
        `¡Enhorabuena, ha salido un ${miDado} ¡Has ganado con un total de ${acum} puntos!`
      );

      break;
    }
  }
}
tiradaDado();
*/
