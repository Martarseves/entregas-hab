"use strict";

//ejercicio 1//

function letterCount(str) {
  let longestWord = str.split(" ").reduce(function (longest, currentWord) {
    return currentWord.length > longest.length ? currentWord : longest;
  }, "");
  console.log(longestWord);
  return longestWord.length;
}

letterCount("Hoy es un dia estupendo y fantastico");

//ejercicio 2//

function BinaryConverter(str) {
  let decimal = +0;
  let bits = +1;
  for (let i = 0; i < str.length; i++) {
    let currNum = +str[str.length - i - 1];
    if (currNum === 1) {
      decimal += bits;
    }
    bits *= 2;
  }
  return decimal;
}
console.log(BinaryConverter("101"));

//ejercicio 3//

function PalindromeTwo(str) {
  const removeSpaces = str.split(" ").join("");
  let word = removeSpaces.length;
  if (word < 2) {
    return true;
  }
  let j = word - 1;
  for (let i = 0; i < j; i++) {
    if (removeSpaces[i].toLowerCase() != removeSpaces[j].toLowerCase()) {
      return false;
    }
    j -= 1;
  }
  return true;
}

console.log(PalindromeTwo("Arriba la birra"));
