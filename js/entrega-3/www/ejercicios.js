"use strict";

/*

EJERCICIO 1: 

Crear un falso e-commerce. Por un lado tenemos todos los artículos que forman
el stock de la tienda.Tendremos también un usuario que añade cosas a su carrito,
que es privado.Los artículos cuando los metemos al carrito los convertimos un tipo de
dato que guarda las unidades del usuario de dicho artículo.La tienda es la
encargada de imprimir un ticket por la consola

*/

const itemNames = ["Camisa", "Pantalon", "Calcetines"];
const itemPrices = [13, 27, 100];

class Item {
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }
  static createItem(names, prices) {
    return names.map((name, index) => {
      return new Item(names[index], prices[index]);
    });
  }
}

class CartItem extends Item {
  ammount = 1;

  constructor(item) {
    super(item.name, item.price);
  }

  increaseAmmount() {
    this.ammount++;
  }

  get total() {
    return this.ammount * this.price;
  }
}

class User {
  #shoppingCart = [];

  addItems(item) {
    const itemFound = this.#shoppingCart.find((cartItem) => {
      return item.name === cartItem.name;
    });
    if (itemFound) {
      itemFound.ammount++;
    } else {
      this.#shoppingCart.push({ itemData: item, ammount: 1 });
    }
  }

  get cart() {
    return this.#shoppingCart;
  }

  fillCart(inventory, ammount) {
    for (let i = 0; i < ammount; i++) {
      const inventoryIndex = Math.floor(Math.random() * inventory.length);
      this.addItems(inventory[inventoryIndex]);
    }
  }
}

class Shop {
  static checkout(shoppingCart) {
    console.log(`Hack a Shop`);

    const initialTotals = { totalPrice: 0, totalUnits: 0 };
    const totals = shoppingCart.reduce((accumulator, currentItem) => {
      console.log(
        `${currentItem.name} ${currentItem.price}€ ${currentItem.ammount}U Total de item: ${currentItem.total}`
      );
      accumulator.totalPrice += currentItem.total;
      accumulator.totalUnits += currentItem.ammount;
      return accumulator;
    }, initialTotals);

    
    );
  }
}

const inventory = itemNames.map((name, index) => {
  return new Item(name, itemPrices[index]);
});

const myUser = new User();
myUser.fillCart(inventory, 50);

/*

EJERCICIO 2: 

Accede con fetch a la información de la siguiente API: https://api.exchangerate-api.com/v4/latest/EUR.
Dado un valor en euros(EUR), convierte dicha cantidad a dólares(USD).Por último convierte el resultado
obtenido en dólares a yenes(JPY).



async function getMoneyExchange(quantity) {
  const ratesInfo = await fetch(
    "https://api.exchangerate-api.com/v4/latest/EUR"
  ).then((response) => {
    return response.json();
  });
  console.log(
    `${quantity}€ = ${Math.round(quantity * ratesInfo.rates.USD * 100) / 100}$.`
  );
  console.log(
    `${quantity}€ = ${Math.round(quantity * ratesInfo.rates.USD * 100) / 100}¥.`
  );
}

getMoneyExchange(prompt(`Euros en Dólares y Yenes respectivamente`));
*/
